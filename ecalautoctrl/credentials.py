from os import environ

dbhost = 'ecal-automation-relay.web.cern.ch'
dbport = 80
dbusr = 'ecalview' if 'ECAL_INFLUXDB_USER' not in environ else environ['ECAL_INFLUXDB_USER']
dbpwd = 'ecalPbWO' if 'ECAL_INFLUXDB_PWD' not in environ else environ['ECAL_INFLUXDB_PWD']
dbssl = False
