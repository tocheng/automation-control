import importlib
# import conddb related classes only if CMSSW is available
try:
    if importlib.util.find_spec('CondCore.Utilities.conddblib'):
        import CondCore.Utilities.conddblib as conddb
        import CondCore.Utilities.conddb_time as conddb_time
except:
    # mock conddblib otherwise (useful for doc generation using sphinx)
    from unittest.mock import MagicMock, patch
    my_conddb = MagicMock()
    patch.dict("sys.modules", cmssw_conddb=my_conddb).start()
    my_conddb_time = MagicMock()
    patch.dict("sys.modules", cmssw_conddb_time=my_conddb_time).start()
    import cmssw_conddb as conddb
    import cmssw_conddb_time as conddb_time
    
from datetime import datetime
from typing import List, Dict, Optional, Union, Tuple
from .TaskHandlers import LockBase

class CondDBLockBase(LockBase):
    """
    Base class for conddb based lock classes. It provides basic
    setup to read the conddb and compare times.

    :param url: full database url or alias as defined in CMSSW.
    """

    def __init__(self, url: Optional[str]='pro', **kwargs):
        super().__init__()
        # un-alias url
        if '//' not in url:
            url = conddb.make_url(url)

        self.session = conddb.connect(url).session()

        self.Payload = self.session.get_dbtype(conddb.Payload)
        self.IOV = self.session.get_dbtype(conddb.IOV)
        self.Tag = self.session.get_dbtype(conddb.Tag)
        self.GlobalTag = self.session.get_dbtype(conddb.GlobalTag)
        self.GlobalTagMap = self.session.get_dbtype(conddb.GlobalTagMap)

        # define a static conddb to automation time variables mapping
        self.timesmap = {
            'Time' : 'endtime',
            'Run' : 'run_number'
            }

    def checkTime(self, runs: List[Dict], payloads: List[Tuple]) -> List[bool]:
        """
        Compare time/run from a conddb payload with the run information.

        :param runs: list of runs to be checked (:class:`~ecalautoctrl.RunCtrl` format).
        :param payloads: list of (since, time_type) values from the tags to be checked.
        :return: a list matching the input list with the result of the check for each input run.
        """

        # convert conddb time into unix time if needed
        payloads = [(t, ttype) if ttype=='Run' else (conddb_time.from_timestamp(t), ttype) for t, ttype in payloads]

        # check for each run that the AND of the condition run_time < payload_since_time.
        return [not all([(int(r[self.timesmap[ttype]]) if ttype=='Run' else datetime.strptime(r[self.timesmap[ttype]], '%Y-%m-%dT%H:%M:%SZ')) < t for t, ttype in payloads]) for r in runs]
        
class CondDBLockGT(CondDBLockBase):
    """
    Implement a lock based on the availability of updated conditions
    in the conddb database. The lock prevents runs to be processed
    if the last available IOV in the specified records as since value
    that is earlier than the run end time.

    :param records: name of the records to monitor.
    :param global_tag: global tag name from which to access the information, if unspecified the global tag will be read from the run dictionary.
    :param url: full database url or alias as defined in CMSSW.
    """

    def __init__(self,
                 records: Union[str, List[str]]=None,
                 global_tag: Optional[str]=None,                 
                 url: Optional[str]='pro',
                 **kwargs):
        super().__init__(url=url, **kwargs)

        self.gt = global_tag
        self.rcds = records if isinstance(records, list) else [records]

    def __str__(self):
        return 'CondDBLockGT'
        
    def lock(self, runs: List[Dict]) -> List[bool]:
        """
        Get for each specified record the tag name specified in the GlobalTag.
        For each tag get the last available IOV and check the each run endtime
        against it.
        
        :param runs: list of runs to be checked (:class:`~ecalautoctrl.RunCtrl` format).
        :return: a list matching the input list with the result of the check for each input run.
        """

        self.gt = runs[-1]['globaltag'] if not self.gt else self.gt
        
        tagnames = [self.session.query(self.GlobalTagMap.record, self.GlobalTagMap.tag_name).filter(self.GlobalTagMap.global_tag_name == self.gt, self.GlobalTagMap.record == rcd).order_by(self.GlobalTagMap.record, self.GlobalTagMap.label).all()[-1][1] for rcd in self.rcds]

        tss = [self.session.query(self.IOV.since, self.Tag.time_type).join(self.IOV.payload).filter(self.IOV.tag_name == tagname, self.Tag.name == tagname).order_by(self.IOV.since.desc(), self.IOV.insertion_time.desc()).limit(1).all()[-1] for tagname in tagnames]

        return self.checkTime(runs=runs, payloads=tss)

class CondDBLockTags(CondDBLockBase):
    """
    Implement a lock based on the availability of updated conditions
    in the conddb database. The lock prevents runs to be processed
    if the last available IOV in the specified records as since value
    that is earlier than the run end time.

    :param url: full database url or alias as defined in CMSSW.
    :param tags: tag names to check against.
    """

    def __init__(self,
                 tags: Union[str, List[str]]=None,
                 url: Optional[str]='pro',
                 **kwargs):
        super().__init__(url=url, **kwargs)

        self.tags = tags if isinstance(tags, list) else [tags]

    def __str__(self):
        return 'CondDBLockTags'
        
    def lock(self, runs: List[Dict]) -> List[bool]:
        """
        For each tag get the last available IOV and check the each run endtime
        against it.
        
        :param runs: list of runs to be checked (:class:`~ecalautoctrl.RunCtrl` format).
        :return: a list matching the input list with the result of the check for each input run.
        """

        tss = [self.session.query(self.IOV.since, self.Tag.time_type).join(self.IOV.payload).filter(self.IOV.tag_name == tagname, self.Tag.name == tagname).order_by(self.IOV.since.desc(), self.IOV.insertion_time.desc()).limit(1).all()[-1] for tagname in self.tags]

        return self.checkTime(runs=runs, payloads=tss)
